HERRAMIENTAS LIBRES PARA EDICIONES

Herramientas para edición de video:

1.- OpenShot: Tiene una interfaz simple de arrastrar y soltar que es fácil de operar incluso para los principiantes.
Brinda funciones avanzadas como capas ilimitadas, transiciones de vídeo con previsualizaciones en tiempo real, etc. Compatible con Mac, Linux y Windows

Herramientas para edición de texto:

1.-  LibreOffice: LibreOffice es una suite ofimática potente y gratuita, sucesora de OpenOffice(.org) , utilizada por millones de personas en todo el mundo.
Su interfaz limpia y sus herramientas ricas en funciones lo ayudan a dar rienda suelta a su creatividad y mejorar su productividad. LibreOffice incluye varias aplicaciones que lo convierten en el paquete ofimático gratuito y de código abierto más versátil del mercado: Writer (procesador de textos), Calc (hojas de cálculo), Impress (presentaciones), Draw (gráficos vectoriales y diagramas de flujo), Base (bases de datos) y Matemáticas (edición de fórmulas).

Herramientas para edición de fotografías:

1.- Gimp: Es uno de los programas de edición más destacados, ya que cuenta con todas las herramientas necesarias para poder realizar el retoque de fotografías.
Una de las grandes ventajas es que es gratuito y muy superior a Photoshop.
