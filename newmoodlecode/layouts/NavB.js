import React from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Outlet, Link } from 'react-router-dom';

const NavB = () => {
    return (
        <>
            <Navbar bg="dark" variant='dark' expand="lg">
                <Container>
                    <Navbar.Brand as={Link} to="/">NEWMOODLE</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link as={Link} to="/">HOME</Nav.Link>
                            <Nav.Link as={Link} to="/signup">SIGNUP</Nav.Link>
                            <Nav.Link as={Link} to="/texteditor">EDITOR DE TEXTO</Nav.Link>
                            <Nav.Link as={Link} to="/imageeditor">EDITOR DE IMAGENES</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

            <section>
                <Outlet></Outlet>
            </section>

        </>
    );
};

export default NavB;