// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAlYHQFeoImoETwZiuF_tJNPkRyZaAhgP0",
  authDomain: "newmoodle-65032.firebaseapp.com",
  projectId: "newmoodle-65032",
  storageBucket: "newmoodle-65032.appspot.com",
  messagingSenderId: "108333608587",
  appId: "1:108333608587:web:c10bc919d9a223d37b6e8e"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);