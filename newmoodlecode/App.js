import React from 'react';
import './App.css';
import Signup from './components/Signup';

function App() {
  return (
    <div className="App">
      <div className='logo-contenedor'>
        <h1>newmoodle</h1>
      </div>
      <div className='contenedor-principal'>
        <Signup />
      </div>
    </div>

  );
}

export default App;
