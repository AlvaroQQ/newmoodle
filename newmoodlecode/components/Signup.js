import React, { useRef } from 'react';
import { Card, Button, Form } from 'react-bootstrap';

export default function Signup() {
    const emailRef = useRef();
    const passwordRef = useRef();
    const passwordConfirmRef = useRef();
    return (
        <>
            <Card>
                <Card.Body>
                    <h2 className='text-center mb-4'>Signup</h2>
                    <Form>
                        <Form.Group id='email'>
                            <Form.Label>Email</Form.Label>
                            <Form.Control type='email' ref={emailRef} required />
                        </Form.Group>
                        <Form.Group id='password'>
                            <Form.Label>Password</Form.Label>
                            <Form.Control type='password' ref={passwordRef} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Password Confirmation</Form.Label>
                            <Form.Control type='password' ref={passwordConfirmRef} required />
                        </Form.Group>
                        <Button className='w-100 text-center mt-2'>Registrarse</Button>
                    </Form>
                </Card.Body>
            </Card>
            <div className='w-100 text-center mt-2' >
                Tiene una cuenta ? Log In
            </div>
        </>
    )
}
